import { Injectable } from '@nestjs/common';
import { User } from './user';
import { UserTokenService } from './token/user-token.service';

@Injectable()
export class UserService {
  private users = [
    new User('admin@admin.ru', '12345678'),
    new User('user@user.ru', '87654321'),
  ];

  constructor(
    private readonly userTokenService: UserTokenService,
  ) {
  }

  getTokenForUser(user: User): string {
    const isAuth = this.isAuthenticated(user);

    if (isAuth) {
      return this.userTokenService.generateToken();
    }
    return null;
  }

  isAuthenticated(data: User): boolean {
    const result = this.users.find((_: User) => {
      return _.email === data.email && _.password === data.password;
    });

    return !!result;
  }
}
