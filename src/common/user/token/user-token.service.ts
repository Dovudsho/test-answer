import { Injectable } from '@nestjs/common';
import { generate } from 'randomstring';

@Injectable()
export class UserTokenService {
  generateToken(): string {
    return generate();
  }
}
