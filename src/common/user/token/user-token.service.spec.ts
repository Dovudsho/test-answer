import { UserTokenService } from './user-token.service';

describe('UserTokenService', () => {
  let userTokenService: UserTokenService;

  beforeAll(async () => {
    userTokenService = new UserTokenService();
  });

  it('should be return TOKEN { String }', () => {
    expect(typeof userTokenService.generateToken() === 'string')
      .toBe(true);
  });
});
