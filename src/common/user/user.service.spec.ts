import { UserService } from './user.service';
import { Test } from '@nestjs/testing';
import { UserTokenService } from './token/user-token.service';
import { User } from './user';
import { generate } from 'randomstring';

describe('UserService', () => {
  let userService: UserService;
  let userTokenService: UserTokenService;

  const authUser = new User('admin@admin.ru', '12345678');

  beforeAll(async () => {
    const moduleRef = await Test.createTestingModule({
      providers: [UserService, UserTokenService],
    }).compile();

    userService = moduleRef.get<UserService>(UserService);
    userTokenService = moduleRef.get<UserTokenService>(UserTokenService);
  });

  it('should be return TOKEN { String } if user is authenticated', () => {
    const generatedToken = generate();
    jest.spyOn(userTokenService, 'generateToken').mockImplementation(() => generatedToken);

    expect(userService.getTokenForUser(authUser)).toBe(generatedToken);
  });

  it('should be return TRUE { Boolean } if user is authenticated', () => {
    expect(userService.isAuthenticated(authUser)).toBe(true);
  });
});
