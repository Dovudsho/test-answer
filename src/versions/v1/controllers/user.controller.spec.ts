import { UserController } from './user.controller';
import { Test } from '@nestjs/testing';
import { UserService } from '../../../common/user/user.service';
import { UserTokenService } from '../../../common/user/token/user-token.service';
import { User } from '../../../common/user/user';
import { generate } from 'randomstring';
import { UserToken } from '../../../common/user/token/user-token';

describe('UserController', () => {
  let userController: UserController;
  let userTokenService: UserTokenService;

  const authUser = new User('admin@admin.ru', '12345678');

  beforeAll(async () => {
    const moduleRef = await Test.createTestingModule({
      controllers: [UserController],
      providers: [UserService, UserTokenService],
    }).compile();

    userController = moduleRef.get<UserController>(UserController);
    userTokenService = moduleRef.get<UserTokenService>(UserTokenService);
  });

  it('should be return object { UserToken } if user is authenticated', async () => {
    const generatedToken = generate();
    jest.spyOn(userTokenService, 'generateToken').mockImplementation(() => generatedToken);

    expect(await userController.login(authUser)).toEqual(new UserToken(generatedToken));
  });
});
