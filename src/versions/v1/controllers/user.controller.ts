import { Body, Controller, HttpException, HttpStatus, Post } from '@nestjs/common';
import { UserService } from '../../../common/user/user.service';
import { User } from '../../../common/user/user';
import { UserToken } from '../../../common/user/token/user-token';

@Controller('v1/user')
export class UserController {
  constructor(
    private readonly userService: UserService,
  ) {
  }

  @Post('login')
  login(@Body() user: User) {
    const token = this.userService.getTokenForUser(user);

    if (token) {
      return new UserToken(token);
    }
    throw new HttpException('Unauthorized', HttpStatus.UNAUTHORIZED);
  }
}
